# IO.Swagger.Model.GetCharactersCharacterIdStatsIsk
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_In** | **long?** | in integer | [optional] 
**_Out** | **long?** | out integer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

