﻿using System;
using System.IO;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;
using System.Collections.Generic;
using System.Net;
using LumenWorks.Framework.IO.Csv;
using System.Globalization;

namespace UndercutCheck
{
    internal static class Consts
    {
        //Search Categories
        internal const string agent = "agent";
        internal const string alliance = "alliance";
        internal const string character = "character";
        internal const string constellation = "constellation";
        internal const string corporation = "corporation";
        internal const string faction = "faction";
        internal const string inventory_type = "inventory_type";
        internal const string region = "region";
        internal const string solar_system = "solar_system";
        internal const string station = "station";
        //Languages
        internal const string german = "de";
        internal const string english = "en-us";
        internal const string french = "fr";
        internal const string japanese = "ja";
        internal const string russian = "ru";
        internal const string zh = "zh";//chinese
        //Data Sources
        internal const string live = "tranquility";
        internal const string test = "singularity";
        //Paths
        internal const string csv_path = "\\invTypes.csv";
        internal const string previous_path = "\\prev.dat";
        //Authentication
        internal const string loginServer = "login.eveonline.com";
        internal const string responseType = "token";
        internal const string access_token = "access_token";
        internal const string scope = "esi-markets.read_corporation_orders.v1";//example scope, authentication requires scope request
        //Defaults
        internal const string the_forge = "The Forge";//default region for searches
        internal const string harkonen = "Harkonen Enterprises";//default corporation for searches
    }

    internal class Program
    {
        const string clientID = "12345abcdef12345abcdef";//****CHANGE TO YOUR CLIENT ID FROM EVE DEVELOPER SITE
        const string redirectURI = "http://localhost:9879/myapp/";//****CHANGE TO YOUR OWN REDIRECT URI AND PORT FROM EVE DEVELOPER SITE

        string dataSource = string.Empty;
        string ifNoneMatch = string.Empty;//ETag from a previous request. A 304 will be returned if this matches the current ETag (optional), used in search functions
        string language = Consts.english;//http response language or eve in game language
        string previousAccessToken = string.Empty;//stores access token/date loaded from file
        int? defaultPageDisplay = 1;//which page to display on search results
        bool? apiStrict = true;//true if only return search on exact match

        MarketApi apiInstanceMarket = new MarketApi();
        SearchApi apiInstanceSearch = new SearchApi();
        InsuranceApi apiInstanceInsurance = new InsuranceApi();
        DateTime authExpires = new DateTime();      //used with AuthenticateWithBrowser
        List<string> categories = new List<string>(); //used for different searches
        Dictionary<int,string> allItems = new Dictionary<int,string>();
        Dictionary<string,int> allItemsReverse = new Dictionary<string, int>();//reverse lookup
        Dictionary<int?, double?> corpBuyOrders = new Dictionary<int?, double?>();
        Dictionary<int?, double?> corpSellOrders = new Dictionary<int?, double?>();

        public void Run( string corporationOption, string dataSourceOption, string regionOption, string insuranceSearch )
        {
            /*
             *  I recommend extending the project from here
             */

            this.dataSource = dataSourceOption;//setting global datasource because its used throughout Program class
            LoadItemNamesFromCSV();//fill allItems and allItemsReverse Dictionaries with name lookup by itemID and itemID lookup by name

            

            int? regionID = GetRegionID( regionOption );//Most API calls require regionID, and we usually have a region name, one can look into the Swagger.Test package for more examples of API calls

            if ( insuranceSearch.Equals(string.Empty))//only run if we have insurance to look up
            {
                int itemID = allItemsReverse[insuranceSearch];//we need itemID of the insurance we are looking up, allItems is Dict<int,string> and allItemsReverse is Dict<string,int> for easy lookups, imported from the CSV
                GetInsuranceInfo( itemID, regionID, regionOption );//outputs insurance information of given search
            }

            /*
             *  The following is an example of authentication with browser, and a GetCorpOrders function that uses the access_token retrieved from the login process.
             */
            //string accessToken = GetAccessTokenViaBrowser();//will use previous login auth tokens but saves prev.dat to current directory so rewrite as necessary
            //string accessToken = GetAccessTokenVerifyTokenExpiration();//will request new auth token regardless, stripped this out of the function i was using, many people might only need this function to implement their own check
            //int? corpID = GetCorpID( corporationOption );//Another example API call, unused in the insurance application but listed here for more examples
            //GetCorpOrders( corpID, accessToken );//fills corpBuyOrders and corpSellOrders Dict<int?,double?> with data
        }

        internal bool LoadItemNamesFromCSV()
        {
            string csvItemNamesPath = Environment.CurrentDirectory + Consts.csv_path;
            string csvFileContents = string.Empty;
            try
            {
                if ( File.Exists( csvItemNamesPath ) )
                {
                    using ( CsvReader csv = new CsvReader( new StreamReader( csvItemNamesPath ), true ) )
                    {
                        string[] headers = csv.GetFieldHeaders();
                        while ( csv.ReadNextRecord() )
                        {
                            allItems.Add( int.Parse( csv[0] ), csv[2] );
                            if ( !allItemsReverse.ContainsKey(csv[2]) )
                            {
                                allItemsReverse.Add( csv[2], int.Parse( csv[0] ) );
                            }
                        }
                    }
                }
            }
            catch ( Exception e )
            {
                Console.WriteLine( e );
                return false;
            }
            return true;
        }
        internal string GetAccessTokenViaBrowser()
        {
            /*
             * 
             * on the eve developer website, you will set the redirectURI, mine is "http://localhost:9879/myapp/"
             * this value is located at the beginning of the Run() function in the Program class.
             *
             */

            HttpListener web = new HttpListener();
            web.Prefixes.Add( redirectURI );
            web.Start();//simple listener to consume login callback

            string oauthLoginUrl = "https://" + Consts.loginServer +
                                    "/oauth/authorize?response_type=" + Consts.responseType +
                                    "&redirect_uri=" + redirectURI +
                                    "&client_id=" + clientID +
                                    "&scope=" + Consts.scope;
            System.Diagnostics.Process.Start( oauthLoginUrl );//launch browser for user to login SSO

            //block until we receive auth request
            HttpListenerContext context = web.GetContext();
            HttpListenerResponse response = context.Response;
            //this is the callback html, because the access_token is in fragments( client only ) we have to use javascript to extract fragment data and redirect as parameters
            //so that this program can get the access token.
            string rs = "<!DOCTYPE html>\n";
            rs += "<html>\n";
            rs += "<body>\n";
            rs += "<h2>Redirecting</h2>\n";
            rs += "<script>\n";
            rs += "function getHash() {\n";
            rs += "if (window.location.hash) {\n";
            rs += "var hash = window.location.hash.substring(1);\n";
            rs += "if (hash.length === 0) {\n";
            rs += "return false;\n";
            rs += "} else {\n";
            rs += "return hash;}\n";
            rs += "} else {\n";
            rs += "return false;}}\n";
            rs += "var hash = getHash();\n";
            rs += "var parts = hash.split('=');\n";
            rs += "document.write(parts[1]);\n";
            rs += "window.location.replace( \"" + redirectURI + "/?access_token=\" + parts[1] );\n";
            rs += "</script>\n";
            rs += "</body>\n";
            rs += "</html>\n";
            //writing html to the buffer
            var buffer = System.Text.Encoding.UTF8.GetBytes( rs );
            response.ContentLength64 = buffer.Length;
            var output = response.OutputStream;
            output.Write( buffer, 0, buffer.Length );
            output.Close();
            //because our previous html redirects, we need to block until redirect request comes that contains the access code as parameter instead of fragment
            context = web.GetContext();
            response = context.Response;
            rs = "<html><body>Token retrieved, you may close this window.</body></html>";//display some html to the user so they know the login is complete
            buffer = System.Text.Encoding.UTF8.GetBytes( rs );
            response.ContentLength64 = buffer.Length;
            output = response.OutputStream;
            output.Write( buffer, 0, buffer.Length );
            output.Close();

            web.Stop();//stop the http listener

            return context.Request.QueryString.Get( Consts.access_token );//return the access token to calling function
        }
        internal string GetAccessTokenVerifyTokenExpiration()
        {
            /*
             * Limitations: this uses CurrentDirectory, so make sure we are running in current path
             * 
             * This function will check if the current working directory has an existing auth token and expiration
             * If it does, we use the token from file
             * otherwise, we get a new auth token by launching the login in a web browser
             * 
             */
            string accessTokenFilePath = Environment.CurrentDirectory + Consts.previous_path;//probably a better way that using current directory, but i'll leave that as an excercise to the app developer
            try
            {   //check if file exists and parse it for the values we need
                if ( File.Exists( accessTokenFilePath ) )
                {
                    string[] x = File.ReadAllText( accessTokenFilePath ).Split(',');
                    previousAccessToken = x[0];
                    authExpires = DateTime.Parse(x[1]);
                }
            }
            catch ( Exception e ) { Console.WriteLine( e ); }

            if ( DateTime.Compare( DateTime.Now, authExpires ) > 0 )//checks if the 20 minutes have expired
            {
                authExpires = DateTime.Now.AddMinutes(20);//sets new expiry
                string returnValue = GetAccessTokenViaBrowser();//launch browser and get new access token
                try
                {
                    //maybe obfuscate/encrypt perhaps so anyone with access to local machine can't grab our auth keys and gain access to the same scopes 
                    File.WriteAllText( accessTokenFilePath, returnValue + "," + authExpires );//write access token and expiration to file, expires after 20m
                }
                catch ( Exception e ) { Console.WriteLine( e ); }
                return returnValue;
            }
            return previousAccessToken;//only happens if our auth token isn't expired
        }

        #region API Function Calls
        internal int? GetRegionID( string searchString )
        {
            categories.Add( Consts.region );
            GetSearchOk gOK = apiInstanceSearch.GetSearch( categories, searchString, language, dataSource, ifNoneMatch, language, apiStrict );
            int? x = null;
            if ( gOK != null && gOK.Region != null && gOK.Region.Count > 0 )
            {
                x = gOK.Region[0].Value;
            }
            categories.Clear();
            return x;
        }
        internal int? GetCorpID( string searchString )
        {
            categories.Add( Consts.corporation );
            GetSearchOk gOK = apiInstanceSearch.GetSearch( categories, searchString, language, dataSource, ifNoneMatch, language, apiStrict );
            int? x = null;
            if ( gOK != null && gOK.Corporation != null && gOK.Corporation.Count > 0 )
            {
                x = gOK.Corporation[0].Value;
            }
            categories.Clear();
            return x;
        }
        internal double? GetRelevantMarketPrice( int? regionID, int? typeID, bool isBuyOrder )
        {
            //this function returns the highest buy order or lowest sell order for the given typeID in a region
            List<GetMarketsRegionIdOrders200Ok> gOK = new List<GetMarketsRegionIdOrders200Ok>();
            string orderType = isBuyOrder ? "buy" : "sell";
            gOK = apiInstanceMarket.GetMarketsRegionIdOrders( orderType, regionID, dataSource, ifNoneMatch, defaultPageDisplay, typeID );

            double? returnValue = isBuyOrder ? 0 : double.MaxValue;
            foreach ( GetMarketsRegionIdOrders200Ok order in gOK )
            {
                if ( isBuyOrder )
                {
                    if ( order.Price > returnValue )
                    {
                        returnValue = order.Price;
                    }
                }
                else
                {
                    if ( order.Price < returnValue )
                    {
                        returnValue = order.Price;
                    }
                }
            }
            if ( returnValue == 0 || returnValue == double.MaxValue )
            {
                returnValue = -1;
            }
            return returnValue;
        }
        internal void GetInsuranceInfo( int itemID, int? regionID, string region )
        {
            List<GetInsurancePrices200Ok> iOK = apiInstanceInsurance.GetInsurancePrices( language, dataSource, ifNoneMatch, language );//Get all insurance info
            double platInsuranceCost = 0, platInsurancePayout = 0;//used later in output

            double? marketPrice = GetRelevantMarketPrice( regionID, itemID, false );//gets the market sell order lowest price so we can compare insurance values to market prices

            foreach ( GetInsurancePrices200Ok insurance in iOK )//iterate until we find the insurance item we are looking for
            {
                if ( insurance.TypeId == itemID )
                {
                    platInsuranceCost   = (double)insurance.Levels[5].Cost;
                    platInsurancePayout = (double)insurance.Levels[5].Payout;
                    break;
                }
            }
            
            CultureInfo ci = new CultureInfo( CultureInfo.CurrentCulture.LCID );//for number formatting
            ci.NumberFormat.CurrencyDecimalDigits = 0;
            ci.NumberFormat.CurrencySymbol = "";

            string name = "Name".PadRight( allItems[itemID].Length, ' ');
            string output1 = String.Format( "{0}     {1}       {2}  ---  {3}", name, "Cost".PadLeft(11,' '), "Payout".PadRight( 11, ' ' ), region.PadLeft(11,' ') );

            string cost = platInsuranceCost.ToString( "C0", ci).PadLeft( 11, ' ' );
            string payout = platInsurancePayout.ToString( "C0", ci ).PadRight( 11, ' ' );
            string output2 = String.Format( "{0}  -  {1}   -   {2}  ---  {3}", allItems[itemID], cost, payout, ((double)marketPrice).ToString("C0", ci).PadLeft( 11, ' ' ) );

            Console.WriteLine();
            Console.WriteLine( output1 );
            Console.WriteLine( output2 );
            Console.WriteLine();
        }
        internal void GetCorpOrders( int? corpID, string accessToken )
        {
            /*
             *  This example we use an auth token and fill corpBuyOrders and corpSellOrders Dictionary with prices keyed by itemID
             */
            List<GetCorporationsCorporationIdOrders200Ok> gOK = new List<GetCorporationsCorporationIdOrders200Ok>();
            gOK = apiInstanceMarket.GetCorporationsCorporationIdOrders( corpID, dataSource, ifNoneMatch, defaultPageDisplay, accessToken );
            foreach ( GetCorporationsCorporationIdOrders200Ok order in gOK )
            {
                //TODO: Add all of same orders, currently we just add the first
                if ( order.IsBuyOrder == true && !corpBuyOrders.ContainsKey( order.TypeId ) )
                {
                    corpBuyOrders.Add( order.TypeId, order.Price );//buys
                }
                else if ( !corpSellOrders.ContainsKey( order.TypeId ) )
                {
                    corpSellOrders.Add( order.TypeId, order.Price );//sells
                }
            }
        }
        #endregion

    }

    public class ConsoleApplication
    {
        public static void Main( string[] args )
        {
            Program p = new Program();
            
            //this section could be re-written, but just a quick way to pass args that doesn't fail gracefully
            #region Override defaults with flags

            string defaultRegion = Consts.the_forge;
            string defaultCorporation = Consts.harkonen;
            string defaultDataSource = Consts.live;
            string defaultInsuranceShip = string.Empty;//only has value if -i flag used

            bool noErrors = true;
            string usageMessage = string.Empty;
            usageMessage += "Usage:                 Default and Options:\n";
            usageMessage += "-d <datasource>        tranquility,singularity\n";
            usageMessage += "-r <region>            The Forge\n";
            usageMessage += "-c <corporation>       Harkonen Enterprises\n";
            usageMessage += "-i <ship_name>         Typhoon, Stabber, Enyo, ...\n";

            for ( int i = 0; i < args.Length; i+=2 )
            {
                string curOption = args[i];
                string curValue = "";
                if ( args.Length >= i + 2 )
                {
                    curValue = args[i + 1];
                }
                switch ( curOption )
                {
                    case "-r":
                        defaultRegion = curValue;
                        break;
                    case "-i":
                        defaultInsuranceShip = curValue;
                        break;
                    case "-c":
                        defaultCorporation = curValue;
                        break;
                    case "-d":
                        defaultDataSource = curValue;
                        break;
                    case "-h":
                    case "-?":
                    case "/?":
                    case "/h":
                    case "-help":
                    case "/help":
                        Console.WriteLine( usageMessage );
                        noErrors = false;//not an error, but we don't want to run the program if just asking for usage
                        break;
                    default:
                        Console.WriteLine( "invalid option" );
                        noErrors = false;
                        break;
                }
            }

            #endregion

            if ( noErrors )
            {
                //moved main sequence to avoid static access issues from Main function
                p.Run( defaultCorporation, defaultDataSource, defaultRegion, defaultInsuranceShip );
            }
        }
    }
}
