/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing ClonesApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class ClonesApiTests
    {
        private ClonesApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new ClonesApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ClonesApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' ClonesApi
            //Assert.IsInstanceOfType(typeof(ClonesApi), instance, "instance is a ClonesApi");
        }

        
        /// <summary>
        /// Test GetCharactersCharacterIdClones
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdClonesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdClones(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdClonesOk> (response, "response is GetCharactersCharacterIdClonesOk");
        }
        
        /// <summary>
        /// Test GetCharactersCharacterIdImplants
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdImplantsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdImplants(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<List<int?>> (response, "response is List<int?>");
        }
        
    }

}
