/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing LocationApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class LocationApiTests
    {
        private LocationApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new LocationApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of LocationApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' LocationApi
            //Assert.IsInstanceOfType(typeof(LocationApi), instance, "instance is a LocationApi");
        }

        
        /// <summary>
        /// Test GetCharactersCharacterIdLocation
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdLocationTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdLocation(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdLocationOk> (response, "response is GetCharactersCharacterIdLocationOk");
        }
        
        /// <summary>
        /// Test GetCharactersCharacterIdOnline
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdOnlineTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdOnline(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdOnlineOk> (response, "response is GetCharactersCharacterIdOnlineOk");
        }
        
        /// <summary>
        /// Test GetCharactersCharacterIdShip
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdShipTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdShip(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdShipOk> (response, "response is GetCharactersCharacterIdShipOk");
        }
        
    }

}
