/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing FactionWarfareApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class FactionWarfareApiTests
    {
        private FactionWarfareApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new FactionWarfareApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of FactionWarfareApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' FactionWarfareApi
            //Assert.IsInstanceOfType(typeof(FactionWarfareApi), instance, "instance is a FactionWarfareApi");
        }

        
        /// <summary>
        /// Test GetCharactersCharacterIdFwStats
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdFwStatsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdFwStats(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdFwStatsOk> (response, "response is GetCharactersCharacterIdFwStatsOk");
        }
        
        /// <summary>
        /// Test GetCorporationsCorporationIdFwStats
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdFwStatsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? corporationId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCorporationsCorporationIdFwStats(corporationId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCorporationsCorporationIdFwStatsOk> (response, "response is GetCorporationsCorporationIdFwStatsOk");
        }
        
        /// <summary>
        /// Test GetFwLeaderboards
        /// </summary>
        [Test]
        public void GetFwLeaderboardsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwLeaderboards(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<GetFwLeaderboardsOk> (response, "response is GetFwLeaderboardsOk");
        }
        
        /// <summary>
        /// Test GetFwLeaderboardsCharacters
        /// </summary>
        [Test]
        public void GetFwLeaderboardsCharactersTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwLeaderboardsCharacters(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<GetFwLeaderboardsCharactersOk> (response, "response is GetFwLeaderboardsCharactersOk");
        }
        
        /// <summary>
        /// Test GetFwLeaderboardsCorporations
        /// </summary>
        [Test]
        public void GetFwLeaderboardsCorporationsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwLeaderboardsCorporations(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<GetFwLeaderboardsCorporationsOk> (response, "response is GetFwLeaderboardsCorporationsOk");
        }
        
        /// <summary>
        /// Test GetFwStats
        /// </summary>
        [Test]
        public void GetFwStatsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwStats(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<List<GetFwStats200Ok>> (response, "response is List<GetFwStats200Ok>");
        }
        
        /// <summary>
        /// Test GetFwSystems
        /// </summary>
        [Test]
        public void GetFwSystemsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwSystems(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<List<GetFwSystems200Ok>> (response, "response is List<GetFwSystems200Ok>");
        }
        
        /// <summary>
        /// Test GetFwWars
        /// </summary>
        [Test]
        public void GetFwWarsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetFwWars(datasource, ifNoneMatch);
            //Assert.IsInstanceOf<List<GetFwWars200Ok>> (response, "response is List<GetFwWars200Ok>");
        }
        
    }

}
