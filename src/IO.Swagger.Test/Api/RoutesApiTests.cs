/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing RoutesApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class RoutesApiTests
    {
        private RoutesApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new RoutesApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of RoutesApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' RoutesApi
            //Assert.IsInstanceOfType(typeof(RoutesApi), instance, "instance is a RoutesApi");
        }

        
        /// <summary>
        /// Test GetRouteOriginDestination
        /// </summary>
        [Test]
        public void GetRouteOriginDestinationTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? destination = null;
            //int? origin = null;
            //List<int?> avoid = null;
            //List<List<int?>> connections = null;
            //string datasource = null;
            //string flag = null;
            //string ifNoneMatch = null;
            //var response = instance.GetRouteOriginDestination(destination, origin, avoid, connections, datasource, flag, ifNoneMatch);
            //Assert.IsInstanceOf<List<int?>> (response, "response is List<int?>");
        }
        
    }

}
