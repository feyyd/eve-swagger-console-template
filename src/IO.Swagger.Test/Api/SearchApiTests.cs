/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing SearchApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class SearchApiTests
    {
        private SearchApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new SearchApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of SearchApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' SearchApi
            //Assert.IsInstanceOfType(typeof(SearchApi), instance, "instance is a SearchApi");
        }

        
        /// <summary>
        /// Test GetCharactersCharacterIdSearch
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdSearchTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> categories = null;
            //int? characterId = null;
            //string search = null;
            //string acceptLanguage = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string language = null;
            //bool? strict = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdSearch(categories, characterId, search, acceptLanguage, datasource, ifNoneMatch, language, strict, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdSearchOk> (response, "response is GetCharactersCharacterIdSearchOk");
        }
        
        /// <summary>
        /// Test GetSearch
        /// </summary>
        [Test]
        public void GetSearchTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<string> categories = null;
            //string search = null;
            //string acceptLanguage = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string language = null;
            //bool? strict = null;
            //var response = instance.GetSearch(categories, search, acceptLanguage, datasource, ifNoneMatch, language, strict);
            //Assert.IsInstanceOf<GetSearchOk> (response, "response is GetSearchOk");
        }
        
    }

}
