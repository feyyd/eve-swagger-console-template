/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using IO.Swagger.Client;
using IO.Swagger.Api;
using IO.Swagger.Model;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PlanetaryInteractionApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class PlanetaryInteractionApiTests
    {
        private PlanetaryInteractionApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new PlanetaryInteractionApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PlanetaryInteractionApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' PlanetaryInteractionApi
            //Assert.IsInstanceOfType(typeof(PlanetaryInteractionApi), instance, "instance is a PlanetaryInteractionApi");
        }

        
        /// <summary>
        /// Test GetCharactersCharacterIdPlanets
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdPlanetsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdPlanets(characterId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<List<GetCharactersCharacterIdPlanets200Ok>> (response, "response is List<GetCharactersCharacterIdPlanets200Ok>");
        }
        
        /// <summary>
        /// Test GetCharactersCharacterIdPlanetsPlanetId
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdPlanetsPlanetIdTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? characterId = null;
            //int? planetId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //string token = null;
            //var response = instance.GetCharactersCharacterIdPlanetsPlanetId(characterId, planetId, datasource, ifNoneMatch, token);
            //Assert.IsInstanceOf<GetCharactersCharacterIdPlanetsPlanetIdOk> (response, "response is GetCharactersCharacterIdPlanetsPlanetIdOk");
        }
        
        /// <summary>
        /// Test GetCorporationsCorporationIdCustomsOffices
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdCustomsOfficesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? corporationId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //int? page = null;
            //string token = null;
            //var response = instance.GetCorporationsCorporationIdCustomsOffices(corporationId, datasource, ifNoneMatch, page, token);
            //Assert.IsInstanceOf<List<GetCorporationsCorporationIdCustomsOffices200Ok>> (response, "response is List<GetCorporationsCorporationIdCustomsOffices200Ok>");
        }
        
        /// <summary>
        /// Test GetUniverseSchematicsSchematicId
        /// </summary>
        [Test]
        public void GetUniverseSchematicsSchematicIdTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? schematicId = null;
            //string datasource = null;
            //string ifNoneMatch = null;
            //var response = instance.GetUniverseSchematicsSchematicId(schematicId, datasource, ifNoneMatch);
            //Assert.IsInstanceOf<GetUniverseSchematicsSchematicIdOk> (response, "response is GetUniverseSchematicsSchematicIdOk");
        }
        
    }

}
