/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetUniverseStarsStarIdOk
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetUniverseStarsStarIdOkTests
    {
        // TODO uncomment below to declare an instance variable for GetUniverseStarsStarIdOk
        //private GetUniverseStarsStarIdOk instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetUniverseStarsStarIdOk
            //instance = new GetUniverseStarsStarIdOk();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetUniverseStarsStarIdOk
        /// </summary>
        [Test]
        public void GetUniverseStarsStarIdOkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetUniverseStarsStarIdOk
            //Assert.IsInstanceOfType<GetUniverseStarsStarIdOk> (instance, "variable 'instance' is a GetUniverseStarsStarIdOk");
        }


        /// <summary>
        /// Test the property 'Age'
        /// </summary>
        [Test]
        public void AgeTest()
        {
            // TODO unit test for the property 'Age'
        }
        /// <summary>
        /// Test the property 'Luminosity'
        /// </summary>
        [Test]
        public void LuminosityTest()
        {
            // TODO unit test for the property 'Luminosity'
        }
        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Radius'
        /// </summary>
        [Test]
        public void RadiusTest()
        {
            // TODO unit test for the property 'Radius'
        }
        /// <summary>
        /// Test the property 'SolarSystemId'
        /// </summary>
        [Test]
        public void SolarSystemIdTest()
        {
            // TODO unit test for the property 'SolarSystemId'
        }
        /// <summary>
        /// Test the property 'SpectralClass'
        /// </summary>
        [Test]
        public void SpectralClassTest()
        {
            // TODO unit test for the property 'SpectralClass'
        }
        /// <summary>
        /// Test the property 'Temperature'
        /// </summary>
        [Test]
        public void TemperatureTest()
        {
            // TODO unit test for the property 'Temperature'
        }
        /// <summary>
        /// Test the property 'TypeId'
        /// </summary>
        [Test]
        public void TypeIdTest()
        {
            // TODO unit test for the property 'TypeId'
        }

    }

}
