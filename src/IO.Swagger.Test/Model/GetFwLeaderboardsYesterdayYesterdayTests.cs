/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetFwLeaderboardsYesterdayYesterday
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetFwLeaderboardsYesterdayYesterdayTests
    {
        // TODO uncomment below to declare an instance variable for GetFwLeaderboardsYesterdayYesterday
        //private GetFwLeaderboardsYesterdayYesterday instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetFwLeaderboardsYesterdayYesterday
            //instance = new GetFwLeaderboardsYesterdayYesterday();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetFwLeaderboardsYesterdayYesterday
        /// </summary>
        [Test]
        public void GetFwLeaderboardsYesterdayYesterdayInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetFwLeaderboardsYesterdayYesterday
            //Assert.IsInstanceOfType<GetFwLeaderboardsYesterdayYesterday> (instance, "variable 'instance' is a GetFwLeaderboardsYesterdayYesterday");
        }


        /// <summary>
        /// Test the property 'Amount'
        /// </summary>
        [Test]
        public void AmountTest()
        {
            // TODO unit test for the property 'Amount'
        }
        /// <summary>
        /// Test the property 'FactionId'
        /// </summary>
        [Test]
        public void FactionIdTest()
        {
            // TODO unit test for the property 'FactionId'
        }

    }

}
