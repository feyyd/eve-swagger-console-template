/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetFwLeaderboardsCorporationsLastWeekLastWeek1
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetFwLeaderboardsCorporationsLastWeekLastWeek1Tests
    {
        // TODO uncomment below to declare an instance variable for GetFwLeaderboardsCorporationsLastWeekLastWeek1
        //private GetFwLeaderboardsCorporationsLastWeekLastWeek1 instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetFwLeaderboardsCorporationsLastWeekLastWeek1
            //instance = new GetFwLeaderboardsCorporationsLastWeekLastWeek1();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetFwLeaderboardsCorporationsLastWeekLastWeek1
        /// </summary>
        [Test]
        public void GetFwLeaderboardsCorporationsLastWeekLastWeek1InstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetFwLeaderboardsCorporationsLastWeekLastWeek1
            //Assert.IsInstanceOfType<GetFwLeaderboardsCorporationsLastWeekLastWeek1> (instance, "variable 'instance' is a GetFwLeaderboardsCorporationsLastWeekLastWeek1");
        }


        /// <summary>
        /// Test the property 'Amount'
        /// </summary>
        [Test]
        public void AmountTest()
        {
            // TODO unit test for the property 'Amount'
        }
        /// <summary>
        /// Test the property 'CorporationId'
        /// </summary>
        [Test]
        public void CorporationIdTest()
        {
            // TODO unit test for the property 'CorporationId'
        }

    }

}
