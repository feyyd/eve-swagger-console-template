/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetUniverseGroupsGroupIdOk
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetUniverseGroupsGroupIdOkTests
    {
        // TODO uncomment below to declare an instance variable for GetUniverseGroupsGroupIdOk
        //private GetUniverseGroupsGroupIdOk instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetUniverseGroupsGroupIdOk
            //instance = new GetUniverseGroupsGroupIdOk();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetUniverseGroupsGroupIdOk
        /// </summary>
        [Test]
        public void GetUniverseGroupsGroupIdOkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetUniverseGroupsGroupIdOk
            //Assert.IsInstanceOfType<GetUniverseGroupsGroupIdOk> (instance, "variable 'instance' is a GetUniverseGroupsGroupIdOk");
        }


        /// <summary>
        /// Test the property 'CategoryId'
        /// </summary>
        [Test]
        public void CategoryIdTest()
        {
            // TODO unit test for the property 'CategoryId'
        }
        /// <summary>
        /// Test the property 'GroupId'
        /// </summary>
        [Test]
        public void GroupIdTest()
        {
            // TODO unit test for the property 'GroupId'
        }
        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Published'
        /// </summary>
        [Test]
        public void PublishedTest()
        {
            // TODO unit test for the property 'Published'
        }
        /// <summary>
        /// Test the property 'Types'
        /// </summary>
        [Test]
        public void TypesTest()
        {
            // TODO unit test for the property 'Types'
        }

    }

}
