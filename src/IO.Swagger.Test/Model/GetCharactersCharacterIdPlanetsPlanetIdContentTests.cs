/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdPlanetsPlanetIdContent
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdPlanetsPlanetIdContentTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdPlanetsPlanetIdContent
        //private GetCharactersCharacterIdPlanetsPlanetIdContent instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdPlanetsPlanetIdContent
            //instance = new GetCharactersCharacterIdPlanetsPlanetIdContent();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdPlanetsPlanetIdContent
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdPlanetsPlanetIdContentInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdPlanetsPlanetIdContent
            //Assert.IsInstanceOfType<GetCharactersCharacterIdPlanetsPlanetIdContent> (instance, "variable 'instance' is a GetCharactersCharacterIdPlanetsPlanetIdContent");
        }


        /// <summary>
        /// Test the property 'Amount'
        /// </summary>
        [Test]
        public void AmountTest()
        {
            // TODO unit test for the property 'Amount'
        }
        /// <summary>
        /// Test the property 'TypeId'
        /// </summary>
        [Test]
        public void TypeIdTest()
        {
            // TODO unit test for the property 'TypeId'
        }

    }

}
