/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetWarsWarIdAlly
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetWarsWarIdAllyTests
    {
        // TODO uncomment below to declare an instance variable for GetWarsWarIdAlly
        //private GetWarsWarIdAlly instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetWarsWarIdAlly
            //instance = new GetWarsWarIdAlly();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetWarsWarIdAlly
        /// </summary>
        [Test]
        public void GetWarsWarIdAllyInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetWarsWarIdAlly
            //Assert.IsInstanceOfType<GetWarsWarIdAlly> (instance, "variable 'instance' is a GetWarsWarIdAlly");
        }


        /// <summary>
        /// Test the property 'AllianceId'
        /// </summary>
        [Test]
        public void AllianceIdTest()
        {
            // TODO unit test for the property 'AllianceId'
        }
        /// <summary>
        /// Test the property 'CorporationId'
        /// </summary>
        [Test]
        public void CorporationIdTest()
        {
            // TODO unit test for the property 'CorporationId'
        }

    }

}
