/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetUniverseConstellationsConstellationIdPosition
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetUniverseConstellationsConstellationIdPositionTests
    {
        // TODO uncomment below to declare an instance variable for GetUniverseConstellationsConstellationIdPosition
        //private GetUniverseConstellationsConstellationIdPosition instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetUniverseConstellationsConstellationIdPosition
            //instance = new GetUniverseConstellationsConstellationIdPosition();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetUniverseConstellationsConstellationIdPosition
        /// </summary>
        [Test]
        public void GetUniverseConstellationsConstellationIdPositionInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetUniverseConstellationsConstellationIdPosition
            //Assert.IsInstanceOfType<GetUniverseConstellationsConstellationIdPosition> (instance, "variable 'instance' is a GetUniverseConstellationsConstellationIdPosition");
        }


        /// <summary>
        /// Test the property 'X'
        /// </summary>
        [Test]
        public void XTest()
        {
            // TODO unit test for the property 'X'
        }
        /// <summary>
        /// Test the property 'Y'
        /// </summary>
        [Test]
        public void YTest()
        {
            // TODO unit test for the property 'Y'
        }
        /// <summary>
        /// Test the property 'Z'
        /// </summary>
        [Test]
        public void ZTest()
        {
            // TODO unit test for the property 'Z'
        }

    }

}
