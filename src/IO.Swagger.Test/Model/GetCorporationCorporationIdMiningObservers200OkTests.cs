/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationCorporationIdMiningObservers200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationCorporationIdMiningObservers200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationCorporationIdMiningObservers200Ok
        //private GetCorporationCorporationIdMiningObservers200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationCorporationIdMiningObservers200Ok
            //instance = new GetCorporationCorporationIdMiningObservers200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationCorporationIdMiningObservers200Ok
        /// </summary>
        [Test]
        public void GetCorporationCorporationIdMiningObservers200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationCorporationIdMiningObservers200Ok
            //Assert.IsInstanceOfType<GetCorporationCorporationIdMiningObservers200Ok> (instance, "variable 'instance' is a GetCorporationCorporationIdMiningObservers200Ok");
        }


        /// <summary>
        /// Test the property 'LastUpdated'
        /// </summary>
        [Test]
        public void LastUpdatedTest()
        {
            // TODO unit test for the property 'LastUpdated'
        }
        /// <summary>
        /// Test the property 'ObserverId'
        /// </summary>
        [Test]
        public void ObserverIdTest()
        {
            // TODO unit test for the property 'ObserverId'
        }
        /// <summary>
        /// Test the property 'ObserverType'
        /// </summary>
        [Test]
        public void ObserverTypeTest()
        {
            // TODO unit test for the property 'ObserverType'
        }

    }

}
