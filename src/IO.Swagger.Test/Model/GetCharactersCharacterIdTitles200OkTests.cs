/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdTitles200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdTitles200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdTitles200Ok
        //private GetCharactersCharacterIdTitles200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdTitles200Ok
            //instance = new GetCharactersCharacterIdTitles200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdTitles200Ok
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdTitles200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdTitles200Ok
            //Assert.IsInstanceOfType<GetCharactersCharacterIdTitles200Ok> (instance, "variable 'instance' is a GetCharactersCharacterIdTitles200Ok");
        }


        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'TitleId'
        /// </summary>
        [Test]
        public void TitleIdTest()
        {
            // TODO unit test for the property 'TitleId'
        }

    }

}
