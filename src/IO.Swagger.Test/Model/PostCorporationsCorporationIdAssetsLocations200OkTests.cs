/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PostCorporationsCorporationIdAssetsLocations200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PostCorporationsCorporationIdAssetsLocations200OkTests
    {
        // TODO uncomment below to declare an instance variable for PostCorporationsCorporationIdAssetsLocations200Ok
        //private PostCorporationsCorporationIdAssetsLocations200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PostCorporationsCorporationIdAssetsLocations200Ok
            //instance = new PostCorporationsCorporationIdAssetsLocations200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PostCorporationsCorporationIdAssetsLocations200Ok
        /// </summary>
        [Test]
        public void PostCorporationsCorporationIdAssetsLocations200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PostCorporationsCorporationIdAssetsLocations200Ok
            //Assert.IsInstanceOfType<PostCorporationsCorporationIdAssetsLocations200Ok> (instance, "variable 'instance' is a PostCorporationsCorporationIdAssetsLocations200Ok");
        }


        /// <summary>
        /// Test the property 'ItemId'
        /// </summary>
        [Test]
        public void ItemIdTest()
        {
            // TODO unit test for the property 'ItemId'
        }
        /// <summary>
        /// Test the property 'Position'
        /// </summary>
        [Test]
        public void PositionTest()
        {
            // TODO unit test for the property 'Position'
        }

    }

}
