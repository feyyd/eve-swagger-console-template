/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetLoyaltyStoresCorporationIdOffersRequiredItem
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetLoyaltyStoresCorporationIdOffersRequiredItemTests
    {
        // TODO uncomment below to declare an instance variable for GetLoyaltyStoresCorporationIdOffersRequiredItem
        //private GetLoyaltyStoresCorporationIdOffersRequiredItem instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetLoyaltyStoresCorporationIdOffersRequiredItem
            //instance = new GetLoyaltyStoresCorporationIdOffersRequiredItem();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetLoyaltyStoresCorporationIdOffersRequiredItem
        /// </summary>
        [Test]
        public void GetLoyaltyStoresCorporationIdOffersRequiredItemInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetLoyaltyStoresCorporationIdOffersRequiredItem
            //Assert.IsInstanceOfType<GetLoyaltyStoresCorporationIdOffersRequiredItem> (instance, "variable 'instance' is a GetLoyaltyStoresCorporationIdOffersRequiredItem");
        }


        /// <summary>
        /// Test the property 'Quantity'
        /// </summary>
        [Test]
        public void QuantityTest()
        {
            // TODO unit test for the property 'Quantity'
        }
        /// <summary>
        /// Test the property 'TypeId'
        /// </summary>
        [Test]
        public void TypeIdTest()
        {
            // TODO unit test for the property 'TypeId'
        }

    }

}
