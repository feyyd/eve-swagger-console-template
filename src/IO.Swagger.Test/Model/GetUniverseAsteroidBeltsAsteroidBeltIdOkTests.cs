/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetUniverseAsteroidBeltsAsteroidBeltIdOk
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetUniverseAsteroidBeltsAsteroidBeltIdOkTests
    {
        // TODO uncomment below to declare an instance variable for GetUniverseAsteroidBeltsAsteroidBeltIdOk
        //private GetUniverseAsteroidBeltsAsteroidBeltIdOk instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetUniverseAsteroidBeltsAsteroidBeltIdOk
            //instance = new GetUniverseAsteroidBeltsAsteroidBeltIdOk();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetUniverseAsteroidBeltsAsteroidBeltIdOk
        /// </summary>
        [Test]
        public void GetUniverseAsteroidBeltsAsteroidBeltIdOkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetUniverseAsteroidBeltsAsteroidBeltIdOk
            //Assert.IsInstanceOfType<GetUniverseAsteroidBeltsAsteroidBeltIdOk> (instance, "variable 'instance' is a GetUniverseAsteroidBeltsAsteroidBeltIdOk");
        }


        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Position'
        /// </summary>
        [Test]
        public void PositionTest()
        {
            // TODO unit test for the property 'Position'
        }
        /// <summary>
        /// Test the property 'SystemId'
        /// </summary>
        [Test]
        public void SystemIdTest()
        {
            // TODO unit test for the property 'SystemId'
        }

    }

}
