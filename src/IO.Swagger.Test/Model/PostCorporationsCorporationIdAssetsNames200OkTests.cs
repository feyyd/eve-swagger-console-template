/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing PostCorporationsCorporationIdAssetsNames200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PostCorporationsCorporationIdAssetsNames200OkTests
    {
        // TODO uncomment below to declare an instance variable for PostCorporationsCorporationIdAssetsNames200Ok
        //private PostCorporationsCorporationIdAssetsNames200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PostCorporationsCorporationIdAssetsNames200Ok
            //instance = new PostCorporationsCorporationIdAssetsNames200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PostCorporationsCorporationIdAssetsNames200Ok
        /// </summary>
        [Test]
        public void PostCorporationsCorporationIdAssetsNames200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PostCorporationsCorporationIdAssetsNames200Ok
            //Assert.IsInstanceOfType<PostCorporationsCorporationIdAssetsNames200Ok> (instance, "variable 'instance' is a PostCorporationsCorporationIdAssetsNames200Ok");
        }


        /// <summary>
        /// Test the property 'ItemId'
        /// </summary>
        [Test]
        public void ItemIdTest()
        {
            // TODO unit test for the property 'ItemId'
        }
        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }

    }

}
