/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationsCorporationIdStandings200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationsCorporationIdStandings200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationsCorporationIdStandings200Ok
        //private GetCorporationsCorporationIdStandings200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationsCorporationIdStandings200Ok
            //instance = new GetCorporationsCorporationIdStandings200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationsCorporationIdStandings200Ok
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdStandings200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationsCorporationIdStandings200Ok
            //Assert.IsInstanceOfType<GetCorporationsCorporationIdStandings200Ok> (instance, "variable 'instance' is a GetCorporationsCorporationIdStandings200Ok");
        }


        /// <summary>
        /// Test the property 'FromId'
        /// </summary>
        [Test]
        public void FromIdTest()
        {
            // TODO unit test for the property 'FromId'
        }
        /// <summary>
        /// Test the property 'FromType'
        /// </summary>
        [Test]
        public void FromTypeTest()
        {
            // TODO unit test for the property 'FromType'
        }
        /// <summary>
        /// Test the property 'Standing'
        /// </summary>
        [Test]
        public void StandingTest()
        {
            // TODO unit test for the property 'Standing'
        }

    }

}
