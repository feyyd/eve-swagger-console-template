/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetFwStatsKills
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetFwStatsKillsTests
    {
        // TODO uncomment below to declare an instance variable for GetFwStatsKills
        //private GetFwStatsKills instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetFwStatsKills
            //instance = new GetFwStatsKills();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetFwStatsKills
        /// </summary>
        [Test]
        public void GetFwStatsKillsInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetFwStatsKills
            //Assert.IsInstanceOfType<GetFwStatsKills> (instance, "variable 'instance' is a GetFwStatsKills");
        }


        /// <summary>
        /// Test the property 'LastWeek'
        /// </summary>
        [Test]
        public void LastWeekTest()
        {
            // TODO unit test for the property 'LastWeek'
        }
        /// <summary>
        /// Test the property 'Total'
        /// </summary>
        [Test]
        public void TotalTest()
        {
            // TODO unit test for the property 'Total'
        }
        /// <summary>
        /// Test the property 'Yesterday'
        /// </summary>
        [Test]
        public void YesterdayTest()
        {
            // TODO unit test for the property 'Yesterday'
        }

    }

}
