/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdPlanetsPlanetIdExtractorDetailsTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails
        //private GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails
            //instance = new GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdPlanetsPlanetIdExtractorDetailsInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails
            //Assert.IsInstanceOfType<GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails> (instance, "variable 'instance' is a GetCharactersCharacterIdPlanetsPlanetIdExtractorDetails");
        }


        /// <summary>
        /// Test the property 'CycleTime'
        /// </summary>
        [Test]
        public void CycleTimeTest()
        {
            // TODO unit test for the property 'CycleTime'
        }
        /// <summary>
        /// Test the property 'HeadRadius'
        /// </summary>
        [Test]
        public void HeadRadiusTest()
        {
            // TODO unit test for the property 'HeadRadius'
        }
        /// <summary>
        /// Test the property 'Heads'
        /// </summary>
        [Test]
        public void HeadsTest()
        {
            // TODO unit test for the property 'Heads'
        }
        /// <summary>
        /// Test the property 'ProductTypeId'
        /// </summary>
        [Test]
        public void ProductTypeIdTest()
        {
            // TODO unit test for the property 'ProductTypeId'
        }
        /// <summary>
        /// Test the property 'QtyPerCycle'
        /// </summary>
        [Test]
        public void QtyPerCycleTest()
        {
            // TODO unit test for the property 'QtyPerCycle'
        }

    }

}
