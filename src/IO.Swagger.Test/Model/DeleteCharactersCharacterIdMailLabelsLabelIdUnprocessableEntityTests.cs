/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntityTests
    {
        // TODO uncomment below to declare an instance variable for DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity
        //private DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity
            //instance = new DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity
        /// </summary>
        [Test]
        public void DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntityInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity
            //Assert.IsInstanceOfType<DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity> (instance, "variable 'instance' is a DeleteCharactersCharacterIdMailLabelsLabelIdUnprocessableEntity");
        }


        /// <summary>
        /// Test the property 'Error'
        /// </summary>
        [Test]
        public void ErrorTest()
        {
            // TODO unit test for the property 'Error'
        }

    }

}
