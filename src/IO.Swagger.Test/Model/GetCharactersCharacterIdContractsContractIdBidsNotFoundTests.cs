/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdContractsContractIdBidsNotFound
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdContractsContractIdBidsNotFoundTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdContractsContractIdBidsNotFound
        //private GetCharactersCharacterIdContractsContractIdBidsNotFound instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdContractsContractIdBidsNotFound
            //instance = new GetCharactersCharacterIdContractsContractIdBidsNotFound();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdContractsContractIdBidsNotFound
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdContractsContractIdBidsNotFoundInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdContractsContractIdBidsNotFound
            //Assert.IsInstanceOfType<GetCharactersCharacterIdContractsContractIdBidsNotFound> (instance, "variable 'instance' is a GetCharactersCharacterIdContractsContractIdBidsNotFound");
        }


        /// <summary>
        /// Test the property 'Error'
        /// </summary>
        [Test]
        public void ErrorTest()
        {
            // TODO unit test for the property 'Error'
        }

    }

}
