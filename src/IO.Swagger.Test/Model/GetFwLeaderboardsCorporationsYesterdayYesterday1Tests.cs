/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetFwLeaderboardsCorporationsYesterdayYesterday1
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetFwLeaderboardsCorporationsYesterdayYesterday1Tests
    {
        // TODO uncomment below to declare an instance variable for GetFwLeaderboardsCorporationsYesterdayYesterday1
        //private GetFwLeaderboardsCorporationsYesterdayYesterday1 instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetFwLeaderboardsCorporationsYesterdayYesterday1
            //instance = new GetFwLeaderboardsCorporationsYesterdayYesterday1();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetFwLeaderboardsCorporationsYesterdayYesterday1
        /// </summary>
        [Test]
        public void GetFwLeaderboardsCorporationsYesterdayYesterday1InstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetFwLeaderboardsCorporationsYesterdayYesterday1
            //Assert.IsInstanceOfType<GetFwLeaderboardsCorporationsYesterdayYesterday1> (instance, "variable 'instance' is a GetFwLeaderboardsCorporationsYesterdayYesterday1");
        }


        /// <summary>
        /// Test the property 'Amount'
        /// </summary>
        [Test]
        public void AmountTest()
        {
            // TODO unit test for the property 'Amount'
        }
        /// <summary>
        /// Test the property 'CorporationId'
        /// </summary>
        [Test]
        public void CorporationIdTest()
        {
            // TODO unit test for the property 'CorporationId'
        }

    }

}
