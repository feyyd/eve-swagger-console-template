/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdWalletJournal200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdWalletJournal200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdWalletJournal200Ok
        //private GetCharactersCharacterIdWalletJournal200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdWalletJournal200Ok
            //instance = new GetCharactersCharacterIdWalletJournal200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdWalletJournal200Ok
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdWalletJournal200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdWalletJournal200Ok
            //Assert.IsInstanceOfType<GetCharactersCharacterIdWalletJournal200Ok> (instance, "variable 'instance' is a GetCharactersCharacterIdWalletJournal200Ok");
        }


        /// <summary>
        /// Test the property 'Amount'
        /// </summary>
        [Test]
        public void AmountTest()
        {
            // TODO unit test for the property 'Amount'
        }
        /// <summary>
        /// Test the property 'Balance'
        /// </summary>
        [Test]
        public void BalanceTest()
        {
            // TODO unit test for the property 'Balance'
        }
        /// <summary>
        /// Test the property 'ContextId'
        /// </summary>
        [Test]
        public void ContextIdTest()
        {
            // TODO unit test for the property 'ContextId'
        }
        /// <summary>
        /// Test the property 'ContextIdType'
        /// </summary>
        [Test]
        public void ContextIdTypeTest()
        {
            // TODO unit test for the property 'ContextIdType'
        }
        /// <summary>
        /// Test the property 'Date'
        /// </summary>
        [Test]
        public void DateTest()
        {
            // TODO unit test for the property 'Date'
        }
        /// <summary>
        /// Test the property 'Description'
        /// </summary>
        [Test]
        public void DescriptionTest()
        {
            // TODO unit test for the property 'Description'
        }
        /// <summary>
        /// Test the property 'FirstPartyId'
        /// </summary>
        [Test]
        public void FirstPartyIdTest()
        {
            // TODO unit test for the property 'FirstPartyId'
        }
        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Test]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
        /// <summary>
        /// Test the property 'Reason'
        /// </summary>
        [Test]
        public void ReasonTest()
        {
            // TODO unit test for the property 'Reason'
        }
        /// <summary>
        /// Test the property 'RefType'
        /// </summary>
        [Test]
        public void RefTypeTest()
        {
            // TODO unit test for the property 'RefType'
        }
        /// <summary>
        /// Test the property 'SecondPartyId'
        /// </summary>
        [Test]
        public void SecondPartyIdTest()
        {
            // TODO unit test for the property 'SecondPartyId'
        }
        /// <summary>
        /// Test the property 'Tax'
        /// </summary>
        [Test]
        public void TaxTest()
        {
            // TODO unit test for the property 'Tax'
        }
        /// <summary>
        /// Test the property 'TaxReceiverId'
        /// </summary>
        [Test]
        public void TaxReceiverIdTest()
        {
            // TODO unit test for the property 'TaxReceiverId'
        }

    }

}
