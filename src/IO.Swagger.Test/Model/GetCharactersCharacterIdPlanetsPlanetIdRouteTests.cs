/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCharactersCharacterIdPlanetsPlanetIdRoute
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCharactersCharacterIdPlanetsPlanetIdRouteTests
    {
        // TODO uncomment below to declare an instance variable for GetCharactersCharacterIdPlanetsPlanetIdRoute
        //private GetCharactersCharacterIdPlanetsPlanetIdRoute instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCharactersCharacterIdPlanetsPlanetIdRoute
            //instance = new GetCharactersCharacterIdPlanetsPlanetIdRoute();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCharactersCharacterIdPlanetsPlanetIdRoute
        /// </summary>
        [Test]
        public void GetCharactersCharacterIdPlanetsPlanetIdRouteInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCharactersCharacterIdPlanetsPlanetIdRoute
            //Assert.IsInstanceOfType<GetCharactersCharacterIdPlanetsPlanetIdRoute> (instance, "variable 'instance' is a GetCharactersCharacterIdPlanetsPlanetIdRoute");
        }


        /// <summary>
        /// Test the property 'ContentTypeId'
        /// </summary>
        [Test]
        public void ContentTypeIdTest()
        {
            // TODO unit test for the property 'ContentTypeId'
        }
        /// <summary>
        /// Test the property 'DestinationPinId'
        /// </summary>
        [Test]
        public void DestinationPinIdTest()
        {
            // TODO unit test for the property 'DestinationPinId'
        }
        /// <summary>
        /// Test the property 'Quantity'
        /// </summary>
        [Test]
        public void QuantityTest()
        {
            // TODO unit test for the property 'Quantity'
        }
        /// <summary>
        /// Test the property 'RouteId'
        /// </summary>
        [Test]
        public void RouteIdTest()
        {
            // TODO unit test for the property 'RouteId'
        }
        /// <summary>
        /// Test the property 'SourcePinId'
        /// </summary>
        [Test]
        public void SourcePinIdTest()
        {
            // TODO unit test for the property 'SourcePinId'
        }
        /// <summary>
        /// Test the property 'Waypoints'
        /// </summary>
        [Test]
        public void WaypointsTest()
        {
            // TODO unit test for the property 'Waypoints'
        }

    }

}
