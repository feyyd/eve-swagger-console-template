/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationsCorporationIdStarbasesStarbaseIdOk
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationsCorporationIdStarbasesStarbaseIdOkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationsCorporationIdStarbasesStarbaseIdOk
        //private GetCorporationsCorporationIdStarbasesStarbaseIdOk instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationsCorporationIdStarbasesStarbaseIdOk
            //instance = new GetCorporationsCorporationIdStarbasesStarbaseIdOk();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationsCorporationIdStarbasesStarbaseIdOk
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdStarbasesStarbaseIdOkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationsCorporationIdStarbasesStarbaseIdOk
            //Assert.IsInstanceOfType<GetCorporationsCorporationIdStarbasesStarbaseIdOk> (instance, "variable 'instance' is a GetCorporationsCorporationIdStarbasesStarbaseIdOk");
        }


        /// <summary>
        /// Test the property 'AllowAllianceMembers'
        /// </summary>
        [Test]
        public void AllowAllianceMembersTest()
        {
            // TODO unit test for the property 'AllowAllianceMembers'
        }
        /// <summary>
        /// Test the property 'AllowCorporationMembers'
        /// </summary>
        [Test]
        public void AllowCorporationMembersTest()
        {
            // TODO unit test for the property 'AllowCorporationMembers'
        }
        /// <summary>
        /// Test the property 'Anchor'
        /// </summary>
        [Test]
        public void AnchorTest()
        {
            // TODO unit test for the property 'Anchor'
        }
        /// <summary>
        /// Test the property 'AttackIfAtWar'
        /// </summary>
        [Test]
        public void AttackIfAtWarTest()
        {
            // TODO unit test for the property 'AttackIfAtWar'
        }
        /// <summary>
        /// Test the property 'AttackIfOtherSecurityStatusDropping'
        /// </summary>
        [Test]
        public void AttackIfOtherSecurityStatusDroppingTest()
        {
            // TODO unit test for the property 'AttackIfOtherSecurityStatusDropping'
        }
        /// <summary>
        /// Test the property 'AttackSecurityStatusThreshold'
        /// </summary>
        [Test]
        public void AttackSecurityStatusThresholdTest()
        {
            // TODO unit test for the property 'AttackSecurityStatusThreshold'
        }
        /// <summary>
        /// Test the property 'AttackStandingThreshold'
        /// </summary>
        [Test]
        public void AttackStandingThresholdTest()
        {
            // TODO unit test for the property 'AttackStandingThreshold'
        }
        /// <summary>
        /// Test the property 'FuelBayTake'
        /// </summary>
        [Test]
        public void FuelBayTakeTest()
        {
            // TODO unit test for the property 'FuelBayTake'
        }
        /// <summary>
        /// Test the property 'FuelBayView'
        /// </summary>
        [Test]
        public void FuelBayViewTest()
        {
            // TODO unit test for the property 'FuelBayView'
        }
        /// <summary>
        /// Test the property 'Fuels'
        /// </summary>
        [Test]
        public void FuelsTest()
        {
            // TODO unit test for the property 'Fuels'
        }
        /// <summary>
        /// Test the property 'Offline'
        /// </summary>
        [Test]
        public void OfflineTest()
        {
            // TODO unit test for the property 'Offline'
        }
        /// <summary>
        /// Test the property 'Online'
        /// </summary>
        [Test]
        public void OnlineTest()
        {
            // TODO unit test for the property 'Online'
        }
        /// <summary>
        /// Test the property 'Unanchor'
        /// </summary>
        [Test]
        public void UnanchorTest()
        {
            // TODO unit test for the property 'Unanchor'
        }
        /// <summary>
        /// Test the property 'UseAllianceStandings'
        /// </summary>
        [Test]
        public void UseAllianceStandingsTest()
        {
            // TODO unit test for the property 'UseAllianceStandings'
        }

    }

}
