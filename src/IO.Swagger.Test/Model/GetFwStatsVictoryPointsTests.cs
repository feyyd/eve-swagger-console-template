/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetFwStatsVictoryPoints
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetFwStatsVictoryPointsTests
    {
        // TODO uncomment below to declare an instance variable for GetFwStatsVictoryPoints
        //private GetFwStatsVictoryPoints instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetFwStatsVictoryPoints
            //instance = new GetFwStatsVictoryPoints();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetFwStatsVictoryPoints
        /// </summary>
        [Test]
        public void GetFwStatsVictoryPointsInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetFwStatsVictoryPoints
            //Assert.IsInstanceOfType<GetFwStatsVictoryPoints> (instance, "variable 'instance' is a GetFwStatsVictoryPoints");
        }


        /// <summary>
        /// Test the property 'LastWeek'
        /// </summary>
        [Test]
        public void LastWeekTest()
        {
            // TODO unit test for the property 'LastWeek'
        }
        /// <summary>
        /// Test the property 'Total'
        /// </summary>
        [Test]
        public void TotalTest()
        {
            // TODO unit test for the property 'Total'
        }
        /// <summary>
        /// Test the property 'Yesterday'
        /// </summary>
        [Test]
        public void YesterdayTest()
        {
            // TODO unit test for the property 'Yesterday'
        }

    }

}
