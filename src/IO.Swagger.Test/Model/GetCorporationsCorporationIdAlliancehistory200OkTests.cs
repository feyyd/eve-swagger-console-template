/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationsCorporationIdAlliancehistory200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationsCorporationIdAlliancehistory200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationsCorporationIdAlliancehistory200Ok
        //private GetCorporationsCorporationIdAlliancehistory200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationsCorporationIdAlliancehistory200Ok
            //instance = new GetCorporationsCorporationIdAlliancehistory200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationsCorporationIdAlliancehistory200Ok
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdAlliancehistory200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationsCorporationIdAlliancehistory200Ok
            //Assert.IsInstanceOfType<GetCorporationsCorporationIdAlliancehistory200Ok> (instance, "variable 'instance' is a GetCorporationsCorporationIdAlliancehistory200Ok");
        }


        /// <summary>
        /// Test the property 'AllianceId'
        /// </summary>
        [Test]
        public void AllianceIdTest()
        {
            // TODO unit test for the property 'AllianceId'
        }
        /// <summary>
        /// Test the property 'IsDeleted'
        /// </summary>
        [Test]
        public void IsDeletedTest()
        {
            // TODO unit test for the property 'IsDeleted'
        }
        /// <summary>
        /// Test the property 'RecordId'
        /// </summary>
        [Test]
        public void RecordIdTest()
        {
            // TODO unit test for the property 'RecordId'
        }
        /// <summary>
        /// Test the property 'StartDate'
        /// </summary>
        [Test]
        public void StartDateTest()
        {
            // TODO unit test for the property 'StartDate'
        }

    }

}
