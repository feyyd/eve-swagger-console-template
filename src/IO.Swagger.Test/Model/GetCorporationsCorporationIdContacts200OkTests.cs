/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationsCorporationIdContacts200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationsCorporationIdContacts200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationsCorporationIdContacts200Ok
        //private GetCorporationsCorporationIdContacts200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationsCorporationIdContacts200Ok
            //instance = new GetCorporationsCorporationIdContacts200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationsCorporationIdContacts200Ok
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdContacts200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationsCorporationIdContacts200Ok
            //Assert.IsInstanceOfType<GetCorporationsCorporationIdContacts200Ok> (instance, "variable 'instance' is a GetCorporationsCorporationIdContacts200Ok");
        }


        /// <summary>
        /// Test the property 'ContactId'
        /// </summary>
        [Test]
        public void ContactIdTest()
        {
            // TODO unit test for the property 'ContactId'
        }
        /// <summary>
        /// Test the property 'ContactType'
        /// </summary>
        [Test]
        public void ContactTypeTest()
        {
            // TODO unit test for the property 'ContactType'
        }
        /// <summary>
        /// Test the property 'IsWatched'
        /// </summary>
        [Test]
        public void IsWatchedTest()
        {
            // TODO unit test for the property 'IsWatched'
        }
        /// <summary>
        /// Test the property 'LabelIds'
        /// </summary>
        [Test]
        public void LabelIdsTest()
        {
            // TODO unit test for the property 'LabelIds'
        }
        /// <summary>
        /// Test the property 'Standing'
        /// </summary>
        [Test]
        public void StandingTest()
        {
            // TODO unit test for the property 'Standing'
        }

    }

}
