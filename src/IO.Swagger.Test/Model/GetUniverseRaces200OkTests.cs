/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetUniverseRaces200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetUniverseRaces200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetUniverseRaces200Ok
        //private GetUniverseRaces200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetUniverseRaces200Ok
            //instance = new GetUniverseRaces200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetUniverseRaces200Ok
        /// </summary>
        [Test]
        public void GetUniverseRaces200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetUniverseRaces200Ok
            //Assert.IsInstanceOfType<GetUniverseRaces200Ok> (instance, "variable 'instance' is a GetUniverseRaces200Ok");
        }


        /// <summary>
        /// Test the property 'AllianceId'
        /// </summary>
        [Test]
        public void AllianceIdTest()
        {
            // TODO unit test for the property 'AllianceId'
        }
        /// <summary>
        /// Test the property 'Description'
        /// </summary>
        [Test]
        public void DescriptionTest()
        {
            // TODO unit test for the property 'Description'
        }
        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'RaceId'
        /// </summary>
        [Test]
        public void RaceIdTest()
        {
            // TODO unit test for the property 'RaceId'
        }

    }

}
