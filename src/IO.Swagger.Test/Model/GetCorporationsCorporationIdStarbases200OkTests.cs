/* 
 * EVE Swagger Interface
 *
 * An OpenAPI for EVE Online
 *
 * OpenAPI spec version: 0.8.4
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using IO.Swagger.Api;
using IO.Swagger.Model;
using IO.Swagger.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace IO.Swagger.Test
{
    /// <summary>
    ///  Class for testing GetCorporationsCorporationIdStarbases200Ok
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class GetCorporationsCorporationIdStarbases200OkTests
    {
        // TODO uncomment below to declare an instance variable for GetCorporationsCorporationIdStarbases200Ok
        //private GetCorporationsCorporationIdStarbases200Ok instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of GetCorporationsCorporationIdStarbases200Ok
            //instance = new GetCorporationsCorporationIdStarbases200Ok();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of GetCorporationsCorporationIdStarbases200Ok
        /// </summary>
        [Test]
        public void GetCorporationsCorporationIdStarbases200OkInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" GetCorporationsCorporationIdStarbases200Ok
            //Assert.IsInstanceOfType<GetCorporationsCorporationIdStarbases200Ok> (instance, "variable 'instance' is a GetCorporationsCorporationIdStarbases200Ok");
        }


        /// <summary>
        /// Test the property 'MoonId'
        /// </summary>
        [Test]
        public void MoonIdTest()
        {
            // TODO unit test for the property 'MoonId'
        }
        /// <summary>
        /// Test the property 'OnlinedSince'
        /// </summary>
        [Test]
        public void OnlinedSinceTest()
        {
            // TODO unit test for the property 'OnlinedSince'
        }
        /// <summary>
        /// Test the property 'ReinforcedUntil'
        /// </summary>
        [Test]
        public void ReinforcedUntilTest()
        {
            // TODO unit test for the property 'ReinforcedUntil'
        }
        /// <summary>
        /// Test the property 'StarbaseId'
        /// </summary>
        [Test]
        public void StarbaseIdTest()
        {
            // TODO unit test for the property 'StarbaseId'
        }
        /// <summary>
        /// Test the property 'State'
        /// </summary>
        [Test]
        public void StateTest()
        {
            // TODO unit test for the property 'State'
        }
        /// <summary>
        /// Test the property 'SystemId'
        /// </summary>
        [Test]
        public void SystemIdTest()
        {
            // TODO unit test for the property 'SystemId'
        }
        /// <summary>
        /// Test the property 'TypeId'
        /// </summary>
        [Test]
        public void TypeIdTest()
        {
            // TODO unit test for the property 'TypeId'
        }
        /// <summary>
        /// Test the property 'UnanchorAt'
        /// </summary>
        [Test]
        public void UnanchorAtTest()
        {
            // TODO unit test for the property 'UnanchorAt'
        }

    }

}
